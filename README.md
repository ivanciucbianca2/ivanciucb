# PlantMe E-commerce website!

Welcome to the PlantMe Store! This project is an online marketplace where users can buy and sell a wide variety of plants. However, only administrators have the privilege to list plants for sale.

## Features

- **User Registration and Authentication**: Secure user registration and login functionality using Spring Security.
- **Product Catalog**: Browse a diverse range of plants available for purchase.
- **Admin Functionality**: Admins can add, update, and remove plants from the catalog.
- **Responsive Design**: Utilizes Bootstrap for a mobile-friendly design.
- **Dynamic Content**: Thymeleaf is used for rendering dynamic HTML content.

## Technologies Used

- **Spring Boot**
- **Spring Security**
- **Thymeleaf**
- **Bootstrap**
- **HTML & CSS**

## Getting Started

### Prerequisites

- **Java Development Kit (JDK)**: Make sure you have JDK 8 or higher installed.
- **Maven**: This project uses Maven for dependency management.
- **Database**: Set up PostgreSQL database and configure it in the application properties.

### Installation



### Usage

- **Access the Application**:
    Open your web browser and navigate to `http://localhost:8080`.

- **Admin Access**:
    Log in as an admin to access the features for adding, updating, and removing plants.

- **User Registration**:
    Register as a new user to browse and purchase plants.

## Project Structure

- **src/main/java**: Contains the Java source code.
- **src/main/resources**: Contains application properties and templates.
- **src/main/resources/templates**: Thymeleaf templates for the web pages.
- **src/main/resources/static**: Static resources like CSS, JavaScript, and images.

## Key Components

- **Controller**: Handles incoming web requests and defines the application endpoints.
- **Service**: Contains the business logic of the application.
- **Repository**: Interacts with the database.
- **Security Configuration**: Configures Spring Security for authentication and authorization.

## Contributing

Contributions are welcome! Please follow these steps:


## License

This project is licensed under the Scoala Informala de IT.

## Contact

For any questions or inquiries, please contact [ivanciucbianca98@yahoo.com].

