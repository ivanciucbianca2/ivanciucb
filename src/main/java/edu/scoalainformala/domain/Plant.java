package edu.scoalainformala.domain;

import edu.scoalainformala.model.PlantCategory;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "plant", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Plant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    private String description;
    private double price;
    private String imageFile;

    // care instructions
    private String water;
    private String sun;
    private String soil;
    private String temperature;


    @Enumerated(EnumType.STRING)
    @Column(name = "plant_category")
    private PlantCategory plantCategory;
}
