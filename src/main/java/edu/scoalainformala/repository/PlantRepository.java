package edu.scoalainformala.repository;

import edu.scoalainformala.domain.Plant;
import edu.scoalainformala.model.PlantCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository

public interface PlantRepository extends JpaRepository<Plant, Long> {

    Plant findByName(String name);

   Optional<Plant> findById(Long id);

    List<Plant> findAllByOrderByNameAsc();


    @Query("SELECT p FROM Plant p WHERE "
            + "(COALESCE(:plantCategory, p.plantCategory) = p.plantCategory) AND "
            + "(COALESCE(:name, '') = '' OR p.name LIKE %:name%) AND "
            + "(COALESCE(:minPrice, 0) <= p.price) AND "
            + "(COALESCE(:maxPrice, 1e10) >= p.price)")
    List<Plant> findAllFiltered(@Param("plantCategory") PlantCategory plantCategory,
                                @Param("name") String name,
                                @Param("minPrice") Double minPrice,
                                @Param("maxPrice") Double maxPrice);

}
