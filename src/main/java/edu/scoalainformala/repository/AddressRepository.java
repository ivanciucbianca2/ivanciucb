package edu.scoalainformala.repository;

import edu.scoalainformala.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
