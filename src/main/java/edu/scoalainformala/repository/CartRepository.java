package edu.scoalainformala.repository;

import edu.scoalainformala.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    Cart findByUserId(Long userId);
    @Query("SELECT SUM(ci.quantity) FROM CartItem ci WHERE ci.cart.user.id = :userId")
    Integer countItemsByUserId(@Param("userId") Long userId);
}
