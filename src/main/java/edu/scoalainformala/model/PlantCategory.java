package edu.scoalainformala.model;

public enum PlantCategory {
    FLOWER,
    TREE,
    SHRUB,
    HERB
}
