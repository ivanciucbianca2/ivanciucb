package edu.scoalainformala.model;


import lombok.Data;

@Data
public class CartItemModel {
    private Long plantId;
    private String plantName;
    private int quantity;
    private double price;
    private String imageFile;
}
