package edu.scoalainformala.model;


public class FilterForm {
    private PlantCategory plantCategory;
    private String name;
    private Double minPrice;
    private Double maxPrice;

    public FilterForm() {
    }

    public FilterForm(PlantCategory plantCategory, String name, Double minPrice, Double maxPrice) {
        this.plantCategory = plantCategory;
        this.name = name;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }


    public PlantCategory getPlantCategory() {
        return plantCategory;
    }

    public void setPlantCategory(PlantCategory plantCategory) {
        this.plantCategory = plantCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }
}