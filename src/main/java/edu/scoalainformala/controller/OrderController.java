package edu.scoalainformala.controller;

import edu.scoalainformala.domain.Address;
import edu.scoalainformala.domain.Cart;
import edu.scoalainformala.domain.Order;
import edu.scoalainformala.domain.UserModel;
import edu.scoalainformala.service.CartService;
import edu.scoalainformala.service.OrderService;
import edu.scoalainformala.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final UserService userService;
    private final CartService cartService;

    @GetMapping("/order")
    public String getOrderPage(Model model) {
        model.addAttribute("orderRequest", new Order());
        model.addAttribute("addressRequest", new Address());
        return "order";
    }

    @PostMapping("/order")
    public String createOrder(@ModelAttribute("orderRequest") Order order,
                              @ModelAttribute("addressRequest") Address address,
                              Model model) {
        UserModel user = userService.getCurrentUser();
        if (user == null) {
            model.addAttribute("errorMessage", "User not logged in");
            return "error-page";
        }

        try {
            Cart cart = cartService.getOrCreateCartForUser(user.getId());
            if (cart.getCartItems().isEmpty()) {
                model.addAttribute("errorMessage", "Your cart is empty.");
                return "order";
            }

            order.setUser(user);
            order.setAddress(address);

            orderService.createOrder(order, cart.getId(), user);
            return "order-confirmation";
        } catch (RuntimeException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "order";
        }
    }
}
