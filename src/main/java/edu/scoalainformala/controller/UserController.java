package edu.scoalainformala.controller;


import edu.scoalainformala.domain.UserModel;
import edu.scoalainformala.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/register")
    public String getRegisterPage(Model model) {
        model.addAttribute("registerRequest", new UserModel());
        return "register-page";
    }

    @GetMapping("/login")
    public String getLoginPage() {
        return "login-page";
    }


    @PostMapping("/register")
    public String register(@ModelAttribute("registerRequest") UserModel userModel, Model model) {
        System.out.println("register request: " + userModel);
        try {
            UserModel registeredUser = userService.registerUser(userModel.getName(), passwordEncoder.encode(userModel.getPassword()), userModel.getEmail());
            return "redirect:/login";
        } catch (RuntimeException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "error-page";
        }
    }


}
