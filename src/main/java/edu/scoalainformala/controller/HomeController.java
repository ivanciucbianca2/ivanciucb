package edu.scoalainformala.controller;


import edu.scoalainformala.config.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String index(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = "";

        if (principal instanceof CustomUserDetails) {
            name = ((CustomUserDetails) principal).getName();
        }

        model.addAttribute("isLoggedIn", !name.isEmpty());
        model.addAttribute("name", name);
        return "index"; // home page
    }
}
