package edu.scoalainformala.controller;

import edu.scoalainformala.config.CustomUserDetails;
import edu.scoalainformala.model.CartItemModel;
import edu.scoalainformala.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.scoalainformala.util.AuthenticaationUtil.getCurrentUserId;

@Controller
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @GetMapping("/cart")
    public String viewCart(Model model) {
        Long userId = getCurrentUserId();
        if (userId == null) {
            return "redirect:/login"; //
        }

        List<CartItemModel> cartItems = cartService.getCartItems(userId);

        double totalPrice = cartService.calculateTotalPrice(userId);

        model.addAttribute("cartItems", cartItems);
        model.addAttribute("totalPrice", totalPrice);
        return "cart";
    }

    @PostMapping("/cart/add")
    public String addToCart(@ModelAttribute("cartItemRequest") CartItemModel cartItemRequest) {
        Long userId = getCurrentUserId();
        if (userId == null) {
            return "redirect:/login";
        }

        cartService.addCartItem(userId, cartItemRequest.getPlantId(), cartItemRequest.getQuantity());
        return "redirect:/cart";
    }

    @PostMapping("/cart/remove")
    public String removeFromCart(@RequestParam Long plantId) {
        Long userId = getCurrentUserId();
        if (userId == null) {
            return "redirect:/login";
        }

        cartService.removeFromCart(userId, plantId);
        return "redirect:/cart";
    }

    @GetMapping("/cart/count")
    public ResponseEntity<Integer> getCartCount() {
        Long userId = getCurrentUserId();
        if (userId == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        int count = cartService.getCartItemCount(userId);
        return ResponseEntity.ok(count);
    }
}
