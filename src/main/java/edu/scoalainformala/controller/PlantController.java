package edu.scoalainformala.controller;


import edu.scoalainformala.model.FilterForm;
import edu.scoalainformala.domain.Plant;
import edu.scoalainformala.model.PlantCategory;
import edu.scoalainformala.model.CartItemModel;
import edu.scoalainformala.service.PlantService;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/plants")
@RequiredArgsConstructor
public class PlantController extends HomeController {
    private final PlantService plantService;


    @GetMapping("/all")
    public String getAllPlants(Model model) {
        List<Plant> plants = plantService.getAllPlants();
        model.addAttribute("plants", plants);
        model.addAttribute("filterForm", new FilterForm());
        return "plants-list";
    }

    @GetMapping("/all/filter")
    public String getFilteredPlants(@RequestParam(required = false) PlantCategory plantCategory,
                                    @RequestParam(required = false) String name,
                                    @RequestParam(required = false) Double minPrice,
                                    @RequestParam(required = false) Double maxPrice,
                                    Model model) {
        List<Plant> plants;
        if (plantCategory != null || name != null || minPrice != null || maxPrice != null) {
            plants = plantService.getAllPlantsFiltered(plantCategory, name, minPrice, maxPrice);
        } else {
            plants = plantService.getAllPlants();
        }
        model.addAttribute("plants", plants);
        model.addAttribute("plantCategory", plantCategory);
        model.addAttribute("name", name);
        model.addAttribute("minPrice", minPrice);
        model.addAttribute("maxPrice", maxPrice);
        model.addAttribute("cartItemRequest", new CartItemModel());

        return "plants-list";
    }

    @GetMapping("/create")
    public String showCreatePlant(Model model) {
        model.addAttribute("plant", new Plant());
        return "create-plant";
    }  //afisarea unui formular gol

    @PostMapping("/create")
    public String createdPlant(@ModelAttribute("plant") Plant plant) {
        plantService.createPlant(plant);
        return "redirect:/plants/all";

    } //crearea plantei si afisarea ei

    @PostMapping("/update")
    public String updatePlant(@ModelAttribute Plant plant, Model model) {
        Plant createdPlant = plantService.updatePlant(plant);
        return "update-plant"; //denumire fisier HTML
    }


    @GetMapping("/details/{id}")
    public String viewPlantDetails(@PathVariable Long id, Model model) {
        Plant plant = plantService.getPlantById(id);
        if (plant != null) {
            model.addAttribute("plantByName", plant);
        } else {
            model.addAttribute("errorMessage", "Plant not found");
        }
        return "specific-plant";
    }

}

