package edu.scoalainformala.service;

import edu.scoalainformala.domain.Plant;
import edu.scoalainformala.model.PlantCategory;
import edu.scoalainformala.repository.PlantRepository;
import jakarta.persistence.EntityExistsException;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;


import java.util.List;


@Data
@RequiredArgsConstructor
@Service
public class PlantService {
    private final PlantRepository plantRepository;


    public List<Plant> getAllPlants() {
        return plantRepository.findAllByOrderByNameAsc();
    }

    public void createPlant(Plant plant) {
        Plant plantFromDb = plantRepository.findByName(plant.getName());
        if (plantFromDb != null) {
            throw new EntityExistsException("A plant with this name already exists");
        } else {
            plantRepository.save(plant);
        }
    }  //creearea de planta noua si salvarea ei

    public Plant updatePlant(Plant updatedPlant) {
        Plant plantFromDb = plantRepository.findByName(updatedPlant.getName());
        if (plantFromDb != null && !plantFromDb.getId().equals(updatedPlant.getId())) {
            throw new EntityExistsException("A plant with this name already exists");
        } else {
            return plantRepository.save(updatedPlant);
        } //updatarea cu date a plantei
    }

    public List<Plant> getAllPlantsFiltered(PlantCategory plantCategory, String name, Double minPrice, Double maxPrice) {
        return plantRepository.findAllFiltered(plantCategory, name, minPrice, maxPrice);
    }

    public Plant getPlantById(Long id) {
        return plantRepository.findById(id).orElse(null);
    }
}
