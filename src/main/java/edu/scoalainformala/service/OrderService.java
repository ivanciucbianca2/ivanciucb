package edu.scoalainformala.service;

import edu.scoalainformala.domain.Order;
import edu.scoalainformala.domain.OrderItem;
import edu.scoalainformala.domain.Cart;
import edu.scoalainformala.domain.UserModel;
import edu.scoalainformala.repository.CartRepository;
import edu.scoalainformala.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;

    @Transactional
    public Order createOrder(Order order, Long cartId, UserModel user) {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new RuntimeException("Cart not found"));

        List<OrderItem> orderItems = cart.getCartItems().stream().map(cartItem -> {
            OrderItem orderItem = new OrderItem();
            orderItem.setQuantity(cartItem.getQuantity());
            orderItem.setOrder(order);
            orderItem.setPlant(cartItem.getPlant());
            return orderItem;
        }).collect(Collectors.toList());

        order.setUser(user);
        order.setOrderItems(orderItems);
        order.setAddress(order.getAddress());
        return orderRepository.save(order);
    }
}
