package edu.scoalainformala.service;

import edu.scoalainformala.config.CustomUserDetails;
import edu.scoalainformala.domain.UserModel;
import edu.scoalainformala.repository.UserRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserModel registerUser(String name, String password, String email) {
        if (name == null || password == null || email == null) {
            throw new IllegalArgumentException("Name, password, and email must not be null");
        }

        UserModel userModel = new UserModel();
        userModel.setName(name);
        userModel.setPassword(password);
        userModel.setEmail(email);
        userModel.setRole("USER");

        try {
            return userRepository.save(userModel);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("A user with this email and password combination already exists.");
        }
    }

    public Optional<UserModel> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    public UserModel getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            return ((CustomUserDetails) principal).getUser();
        } else {
            throw new RuntimeException("User not found");
        }
    }
}
