package edu.scoalainformala.service;

import edu.scoalainformala.domain.Cart;
import edu.scoalainformala.domain.CartItem;
import edu.scoalainformala.domain.Plant;
import edu.scoalainformala.model.CartItemModel;
import edu.scoalainformala.repository.CartItemRepository;
import edu.scoalainformala.repository.CartRepository;
import edu.scoalainformala.repository.PlantRepository;
import edu.scoalainformala.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CartService {

    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;
    private final PlantRepository plantRepository;
    private final UserRepository userRepository;

    public List<CartItemModel> getCartItems(Long userId) {
        Optional<Cart> cartOptional = Optional.ofNullable(cartRepository.findByUserId(userId));
        if (cartOptional.isPresent()) {
            Cart cart = cartOptional.get();
            return cart.getCartItems().stream()
                    .map(this::convertToModel)
                    .collect(Collectors.toList());
        } else {
            return List.of();
        }
    }

    private CartItemModel convertToModel(CartItem cartItem) {
        CartItemModel model = new CartItemModel();
        model.setPlantId(cartItem.getPlant().getId());
        model.setPlantName(cartItem.getPlant().getName());
        model.setQuantity(cartItem.getQuantity());
        model.setPrice(cartItem.getPlant().getPrice());
        model.setImageFile(cartItem.getPlant().getImageFile());
        return model;
    }

    public void addCartItem(Long userId, Long plantId, int quantity) {
        Optional<Cart> cartOptional = Optional.ofNullable(cartRepository.findByUserId(userId));
        Cart cart;
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
        } else {
            cart = new Cart();
            cart.setUser(userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("Invalid user ID")));
            cart = cartRepository.save(cart);
        }

        Plant plant = plantRepository.findById(plantId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid plant ID"));
        addOrCreateCartItem(plantId, quantity, cart, plant);
    }



    public int getCartItemCount(Long userId) {
        Integer itemCount = cartRepository.countItemsByUserId(userId);
        return itemCount != null ? itemCount : 0;
    }

    public void removeFromCart(Long userId, Long plantId) {
        Optional<Cart> cartOptional = Optional.ofNullable(cartRepository.findByUserId(userId));
        if (cartOptional.isPresent()) {
            Cart cart = cartOptional.get();
            CartItem cartItem = cartItemRepository.findByCartIdAndPlantId(cart.getId(), plantId)
                    .orElseThrow(() -> new RuntimeException("Cart item not found"));

            cartItemRepository.delete(cartItem);
        } else {
            throw new RuntimeException("Cart not found for user");
        }
    }

    public double calculateTotalPrice(Long userId) {
        List<CartItemModel> cartItems = getCartItems(userId);
        return cartItems.stream().mapToDouble(item -> item.getPrice() * item.getQuantity()).sum();
    }

    public Cart getOrCreateCartForUser(Long userId) {
        Cart cart = cartRepository.findByUserId(userId);
        if (cart == null) {
            cart = new Cart();
            cart.setUser(userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("Invalid user ID")));
            cart = cartRepository.save(cart);
        }
        return cart;
    }

    private void addOrCreateCartItem(Long plantId, int quantity, Cart cart, Plant plant) {
        Optional<CartItem> existingCartItem = cartItemRepository.findByCartIdAndPlantId(cart.getId(), plantId);
        if (existingCartItem.isPresent()) {
            CartItem cartItem = existingCartItem.get();
            cartItem.setQuantity(cartItem.getQuantity() + quantity);
            cartItemRepository.save(cartItem);
        } else {
            CartItem cartItem = new CartItem();
            cartItem.setCart(cart);
            cartItem.setPlant(plant);
            cartItem.setQuantity(quantity);
            cartItemRepository.save(cartItem);
        }
    }

}
